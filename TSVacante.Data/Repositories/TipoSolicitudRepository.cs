﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Data;
using TSVacante.Core.Models;


namespace TSVacante.Data.Repositories
{
    public class TipoSolicitudRepository : ITipoSolicitudRepository
    {
        private readonly TSVacanteContext _db;

        public TipoSolicitudRepository(TSVacanteContext db)
        {
            _db = db;
        }

        public async Task<TipoSolicitud> Get(int id)
        {
            return await _db.cat_tipos_solicitud.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<TipoSolicitud>> All()
        {
            return await _db.cat_tipos_solicitud.ToListAsync();
        }

        public async Task<TipoSolicitud> Add(TipoSolicitud tipoSolicitud)
        {
            await _db.cat_tipos_solicitud.AddAsync(tipoSolicitud);
            await _db.SaveChangesAsync();
            return tipoSolicitud;
        }
    }
}

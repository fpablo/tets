﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Data.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly TSVacanteContext _db;

        public UsuarioRepository(TSVacanteContext db)
        {
            _db = db;
        }

        public async Task<Usuario> Get(int id)
        {
            return await _db.usuarios.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<Usuario>> All()
        {
            return await _db.usuarios.ToListAsync();
        }

        public async Task<Usuario> Add(Usuario usuario)
        {
            await _db.usuarios.AddAsync(usuario);
            await _db.SaveChangesAsync();
            return usuario;
        }
    }
}

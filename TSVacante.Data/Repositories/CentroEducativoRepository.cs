﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Data;
using TSVacante.Core.Models;


namespace TSVacante.Data.Repositories
{
    public class CentroEducativoRepository : ICentroEducativoRepository
    {
        private readonly TSVacanteContext _db;

        public CentroEducativoRepository(TSVacanteContext db)
        {
            _db = db;
        }

        public async Task<CentroEducativo> Get(int id)
        {
            return await _db.cat_centros_educativos.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<CentroEducativo>> All()
        {
            return await _db.cat_centros_educativos.ToListAsync();
        }

      public async Task<CentroEducativo> Add(CentroEducativo centroEducativo)
        {
            await _db.cat_centros_educativos.AddAsync(centroEducativo);
            await _db.SaveChangesAsync();
            return centroEducativo;
        }
    }
}

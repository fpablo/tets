﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Data.Repositories
{
    public class SolicitudRepository : ISolicitudRepository
    {
        private readonly TSVacanteContext _db;

        public SolicitudRepository (TSVacanteContext db)
        {
            _db = db;
        }

        public async Task<Solicitud> Get(int id)
        {
            return await _db.Solicitudes.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<Solicitud>> All()
        {
            return await _db.Solicitudes.ToListAsync();
        }

        public async Task<Solicitud> Add(Solicitud solicitud)
        {
            await _db.Solicitudes.AddAsync(solicitud);
            await _db.SaveChangesAsync();
            return solicitud;
        }
    }
}

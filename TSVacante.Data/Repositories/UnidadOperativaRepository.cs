﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Data;
using TSVacante.Core.Models;


namespace TSVacante.Data.Repositories
{
    public class UnidadOperativaRepository : IUnidadOperativaRepository
    {
        private readonly TSVacanteContext _db;

        public UnidadOperativaRepository(TSVacanteContext db)
        {
            _db = db;
        }

        public async Task<UnidadOpetativa> Get(int id)
        {
            return await _db.cat_unidades_operativas.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<UnidadOpetativa>> All()
        {
            return await _db.cat_unidades_operativas.ToListAsync();
        }

        public async Task<UnidadOpetativa> Add(UnidadOpetativa unidadOpetativa)
        {
            await _db.cat_unidades_operativas.AddAsync(unidadOpetativa);
            await _db.SaveChangesAsync();
            return unidadOpetativa;
        }
    }
}

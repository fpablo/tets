﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Data.Repositories
{
    public class AreaRepository : IAreaRepository
    {
        private readonly TSVacanteContext _db;

        public AreaRepository(TSVacanteContext db)
        {
            _db = db;
        }

        public async Task<Area> Get(int id)
        {
            return await _db.cat_areas.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<Area>> All()
        {
            return await _db.cat_areas.ToListAsync();
        }

        public async Task<Area> Add(Area area)
        {
            await _db.cat_areas.AddAsync(area);
            await _db.SaveChangesAsync();
            return area;
        }
    }
}

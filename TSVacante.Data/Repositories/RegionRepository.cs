﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Data.Repositories
{
    public class RegionRepository : IRegionRepository
    {
        private readonly TSVacanteContext _db;

        public RegionRepository(TSVacanteContext db)
        {
            _db = db;
        }

        public async Task<Region> Get(int id)
        {
            return await _db.cat_regiones.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<Region>> All()
        {
            return await _db.cat_regiones.ToListAsync();
        }

        public async Task<Region> Add(Region region)
        {
            await _db.cat_regiones.AddAsync(region);
            await _db.SaveChangesAsync();
            return region;
        }
    }
}

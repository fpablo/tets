﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Data.Repositories
{
    public class EstatusRepository : IEstatusRepository
    {
        private readonly TSVacanteContext _db;

        public EstatusRepository(TSVacanteContext db)
        {
            _db = db;
        }

        public async Task<Estatus> Get(int id)
        {
            return await _db.cat_estatus.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<Estatus>> All()
        {
            return await _db.cat_estatus.ToListAsync();
        }

        public async Task<Estatus> Add(Estatus estatus)
        {
            await _db.cat_estatus.AddAsync(estatus);
            await _db.SaveChangesAsync();
            return estatus;
        }
    }
}

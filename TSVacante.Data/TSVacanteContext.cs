﻿using Microsoft.EntityFrameworkCore;
using TSVacante.Core.Models;
namespace TSVacante.Data
{
    public sealed class TSVacanteContext : DbContext
    {
        public TSVacanteContext(DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Solicitud> Solicitudes { get; set; }
        public DbSet<Area> cat_areas { get; set; }
        public DbSet<UnidadOpetativa> cat_unidades_operativas { get; set; }
        public DbSet<Region> cat_regiones { get; set; }
        public DbSet<CentroEducativo> cat_centros_educativos { get; set; }
        public DbSet<Estatus> cat_estatus { get; set; }
        public DbSet<TipoSolicitud> cat_tipos_solicitud { get; set; }
        public DbSet<Usuario> usuarios { get; set; }
    }
}

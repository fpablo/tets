﻿using Microsoft.EntityFrameworkCore;

namespace TSVacante.Data
{
    public class TemporaryDbContextFactory : DesignTimeDbContextFactoryBase<TSVacanteContext>
    {
        protected override TSVacanteContext CreateNewInstance(
            DbContextOptions<TSVacanteContext> options)
        {
            return new TSVacanteContext(options);
        }
    }
}

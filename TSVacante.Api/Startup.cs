using GraphiQl;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TSVacante.Api.Helpers;
using TSVacante.Api.Models;
using TSVacante.Core.Data;
using TSVacante.Data;
using TSVacante.Data.Repositories;

namespace TSVacante.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllers();
            services.AddMvc(options => options.EnableEndpointRouting = false);
            services.AddHttpContextAccessor();
            services.AddSingleton<ContextServiceLocator>();
            services.AddDbContext<TSVacanteContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SQLServerConnection")));
            services.AddTransient<ISolicitudRepository, SolicitudRepository>();
            services.AddTransient<IAreaRepository, AreaRepository>();
            services.AddTransient<IUnidadOperativaRepository, UnidadOperativaRepository>();
            services.AddTransient<IRegionRepository, RegionRepository>();
            services.AddTransient<ICentroEducativoRepository, CentroEducativoRepository>();
            services.AddTransient<IEstatusRepository, EstatusRepository>();
            services.AddTransient<ITipoSolicitudRepository, TipoSolicitudRepository>();
            services.AddTransient<IUsuarioRepository, UsuarioRepository>();

            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();
            services.AddSingleton<TSVacanteQuery>();
            services.AddSingleton<TSVacanteMutation>();

            services.AddSingleton<SolicitudType>();
            services.AddSingleton<AreaType>();
            services.AddSingleton<UnidadOperativaType>();
            services.AddSingleton<RegionType>();
            services.AddSingleton<CentroEducativoType>();
            services.AddSingleton<EstatusType>();
            services.AddSingleton<TipoSolicitudType>();
            services.AddSingleton<UsuarioType>();

            services.AddSingleton<SolicitudInputType>();
            services.AddSingleton<AreaInputType>();
            services.AddSingleton<UnidadOperativaInputType>();
            services.AddSingleton<RegionInputType>();
            services.AddSingleton<CentroEducativoInputType>();
            services.AddSingleton<EstatusInputType>();
            services.AddSingleton<TipoSolicitudInputType>();
            services.AddSingleton<UsuarioInputType>();

            var sp = services.BuildServiceProvider();
            services.AddSingleton<ISchema>(new TSVacanteSchema(new FuncDependencyResolver(type => sp.GetService(type))));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, TSVacanteContext db)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseGraphiQl();
            app.UseMvc();

        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using TSVacante.Core.Data;

namespace TSVacante.Api.Helpers
{
    // https://github.com/graphql-dotnet/graphql-dotnet/issues/648#issuecomment-431489339
    public class ContextServiceLocator
    {
        public ISolicitudRepository SolicitudesRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<ISolicitudRepository>();
        public IAreaRepository AreaRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<IAreaRepository>();
        public IUnidadOperativaRepository UnidadOperativaRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<IUnidadOperativaRepository>();
        public IRegionRepository RegionRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<IRegionRepository>();
        public ICentroEducativoRepository CentroEducativoRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<ICentroEducativoRepository>();
        public IEstatusRepository EstatusRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<IEstatusRepository>();
        public ITipoSolicitudRepository TipoSolicitudRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<ITipoSolicitudRepository>();
        public IUsuarioRepository UsuarioRepository => _httpContextAccessor.HttpContext.RequestServices.GetRequiredService<IUsuarioRepository>();

        private readonly IHttpContextAccessor _httpContextAccessor;

        public ContextServiceLocator(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
    }
}

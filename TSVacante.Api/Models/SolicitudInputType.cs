﻿using GraphQL.Types;


namespace TSVacante.Api.Models
{
    public class SolicitudInputType : InputObjectGraphType
    {
        public SolicitudInputType()
        {
            Name = "SolicitudInput";
            Field<NonNullGraphType<IntGraphType>>("id");
            Field<NonNullGraphType<StringGraphType>>("nombre_puesto");
            Field<NonNullGraphType<IntGraphType>>("area_id");
            Field<NonNullGraphType<StringGraphType>>("departamento");
            Field<NonNullGraphType<StringGraphType>>("puesto_reporta");
            Field<NonNullGraphType<StringGraphType>>("correo_jefe_directo");
            Field<NonNullGraphType<IntGraphType>>("ou_id");
            Field<NonNullGraphType<IntGraphType>>("region_id");
            Field<NonNullGraphType<IntGraphType>>("centro_id");

            Field<NonNullGraphType<StringGraphType>>("justificacion_operativa");
            Field<NonNullGraphType<StringGraphType>>("justificacion_estrategica");
            Field<NonNullGraphType<BooleanGraphType>>("en_presupuesto");
            Field<NonNullGraphType<StringGraphType>>("objetivo_puesto");
            Field<NonNullGraphType<StringGraphType>>("carrera");
            //Field<NonNullGraphType<IntGraphType>>("experiencia_minima");
            Field<NonNullGraphType<StringGraphType>>("conocimientos_tecnicos");
            Field<NonNullGraphType<StringGraphType>>("sector_preferencia");
            Field<NonNullGraphType<IntGraphType>>("edad");
            Field<NonNullGraphType<StringGraphType>>("equipos");
            Field<NonNullGraphType<StringGraphType>>("comportamientos_mes_1");
            Field<NonNullGraphType<StringGraphType>>("aspectos_descarte");
            Field<NonNullGraphType<DateTimeGraphType>>("fecha");
            Field<NonNullGraphType<DateTimeGraphType>>("fecha_actualizacion");
            //Field<NonNullGraphType<FloatGraphType>>("sueldo_propuesto");
            Field<NonNullGraphType<StringGraphType>>("nivel");
            //Field<NonNullGraphType<FloatGraphType>>("sueldo_autorizado");
            Field<NonNullGraphType<IntGraphType>>("deleted");

            Field<NonNullGraphType<IntGraphType>>("status_id");
            Field<NonNullGraphType<IntGraphType>>("tipo_solicitud_id");
            Field<NonNullGraphType<IntGraphType>>("usuario_id");
        }
    }
}

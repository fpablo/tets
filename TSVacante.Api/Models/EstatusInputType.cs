﻿using GraphQL.Types;

namespace TSVacante.Api.Models
{
    public class EstatusInputType : InputObjectGraphType
    {
        public EstatusInputType()
        {
            Name = "EstatusInput";
            Field<NonNullGraphType<IntGraphType>>("id");
            Field<NonNullGraphType<StringGraphType>>("nombre");
            Field<NonNullGraphType<StringGraphType>>("descripcion");
            Field<NonNullGraphType<StringGraphType>>("codigo_interno");
            Field<NonNullGraphType<IntGraphType>>("status");
            Field<NonNullGraphType<IntGraphType>>("deleted");
        }
    }
}

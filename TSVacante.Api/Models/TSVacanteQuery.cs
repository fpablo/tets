﻿using GraphQL.Types;
using TSVacante.Api.Helpers;
using TSVacante.Core.Data;

namespace TSVacante.Api.Models
{
    public class TSVacanteQuery : ObjectGraphType
    {
        public TSVacanteQuery(ContextServiceLocator contextServiceLocator)
        {
            Field<SolicitudType>(
               "solicitud",
               arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id" }),
               resolve: context => contextServiceLocator.SolicitudesRepository.Get(context.GetArgument<int>("id")));

            Field<ListGraphType<SolicitudType>>(
               "allSolicitudes",
               resolve: context => contextServiceLocator.SolicitudesRepository.All());

            Field<ListGraphType<AreaType>>(
                "allArea",
               resolve: context => contextServiceLocator.AreaRepository.All());

            Field<ListGraphType<UnidadOperativaType>>(
                "allCatOus",
               resolve: context => contextServiceLocator.UnidadOperativaRepository.All());

            Field<ListGraphType<RegionType>>(
                "allCatRegiones",
               resolve: context => contextServiceLocator.RegionRepository.All());

            Field<ListGraphType<CentroEducativoType>>(
                "allCatCentros",
               resolve: context => contextServiceLocator.CentroEducativoRepository.All());

            Field<ListGraphType<EstatusType>>(
                "allCatEstatus",
               resolve: context => contextServiceLocator.EstatusRepository.All());

            Field<ListGraphType<TipoSolicitudType>>(
                "allCatSolicitudTipo",
               resolve: context => contextServiceLocator.TipoSolicitudRepository.All());

            Field<ListGraphType<UsuarioType>>(
                "allUsuarios",
               resolve: context => contextServiceLocator.UsuarioRepository.All());
        }
    }
}

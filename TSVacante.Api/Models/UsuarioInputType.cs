﻿using GraphQL.Types;

namespace TSVacante.Api.Models
{
    public class UsuarioInputType : InputObjectGraphType
    {
        public UsuarioInputType()
        {
            Name = "UsuarioInput";
            Field<NonNullGraphType<IntGraphType>>("id");
            Field<NonNullGraphType<StringGraphType>>("nombre");
            Field<NonNullGraphType<StringGraphType>>("correo");
            Field<NonNullGraphType<IntGraphType>>("status");
            Field<NonNullGraphType<IntGraphType>>("deleted");
        }
    }
}

﻿using GraphQL.Types;
using TSVacante.Api.Helpers;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Api.Models
{
    public class SolicitudType : ObjectGraphType<Solicitud>
    {
        public SolicitudType(ContextServiceLocator contextServiceLocator)
        {
            Field(x => x.Id);
            Field(x => x.Nombre_puesto);
            Field(x => x.Area_id);
            Field(x => x.Departamento);
            Field(x => x.Puesto_reporta);
            Field(x => x.Correo_jefe_directo);

            Field(x => x.Justificacion_operativa);
            Field(x => x.Justificacion_estrategica);
            Field(x => x.En_presupuesto);
            Field(x => x.Objetivo_puesto,true);
            Field(x => x.Carrera, true);
            //Field(x => x.Experiencia_minima, true);
            Field(x => x.Conocimientos_tecnicos, true);
            Field(x => x.Sector_preferencia, true);
            //Field(x => x.Edad, true);
            Field(x => x.Equipos, true);
            Field(x => x.Comportamientos_mes_1,true);
            Field(x => x.Aspectos_descarte,true);
            Field(x => x.Fecha);
            Field(x => x.Fecha_actualizacion);
            //Field(x => x.Sueldo_propuesto,true);
            Field(x => x.Nivel, true);
            //Field(x => x.Sueldo_autorizado,true);
            Field(x => x.Deleted);

            Field<AreaType>("area",
                arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "Id" }),
                resolve: context => contextServiceLocator.AreaRepository.Get(context.Source.Area_id), description: "Datos del area");

            Field<UnidadOperativaType>("ou",
               arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "Id" }),
               resolve: context => contextServiceLocator.UnidadOperativaRepository.Get(context.Source.Ou_id), description: "Unidad Operativa");

            Field<RegionType>("region",
               arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "Id" }),
               resolve: context => contextServiceLocator.RegionRepository.Get(context.Source.Region_id), description: "Region");
            
            Field<CentroEducativoType>("centro",
               arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "Id" }),
               resolve: context => contextServiceLocator.CentroEducativoRepository.Get(context.Source.Centro_id), description: "Centro Educativo");
            
            Field<EstatusType>("status",
              arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "Id" }),
              resolve: context => contextServiceLocator.EstatusRepository.Get(context.Source.Status_id), description: "Estatus");

            Field<TipoSolicitudInputType>("tipo_solicitud",
             arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "Id" }),
             resolve: context => contextServiceLocator.TipoSolicitudRepository.Get(context.Source.Tipo_solicitud_id), description: "Tipo Solicitud");

            Field<UsuarioType>("usuario",
             arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "Id" }),
             resolve: context => contextServiceLocator.UsuarioRepository.Get(context.Source.Usuario_id), description: "Usuario");
        }
    }
}

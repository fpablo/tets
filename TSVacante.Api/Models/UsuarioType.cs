﻿using GraphQL.Types;
using TSVacante.Api.Helpers;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Api.Models
{
    public class UsuarioType : ObjectGraphType<Usuario>
    {
        public UsuarioType(ContextServiceLocator contextServiceLocator)
        {
            Field(x => x.Id);
            Field(x => x.Nombre);
            Field(x => x.Correo);
            Field(x => x.Status);
            Field(x => x.Deleted);
        }
    }
}

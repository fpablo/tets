﻿using GraphQL.Types;

namespace TSVacante.Api.Models
{
    public class TipoSolicitudInputType : InputObjectGraphType
    {
        public TipoSolicitudInputType()
        {
            Name = "TipoSolicitudInput";
            Field<NonNullGraphType<IntGraphType>>("id");
            Field<NonNullGraphType<StringGraphType>>("nombre");
            Field<NonNullGraphType<StringGraphType>>("descripcion");
            Field<NonNullGraphType<StringGraphType>>("codigo_interno");
            Field<NonNullGraphType<IntGraphType>>("status");
            Field<NonNullGraphType<IntGraphType>>("deleted");
        }
    }
}

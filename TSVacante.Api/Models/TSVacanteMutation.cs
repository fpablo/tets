﻿using GraphQL.Types;
using TSVacante.Api.Helpers;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Api.Models
{
    public class TSVacanteMutation : ObjectGraphType
    {
        public TSVacanteMutation (ContextServiceLocator contextServiceLocator)
        {
            Name = "CreateSolicitudMutation";
            Field<SolicitudType>(
                "createSolicitud",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<SolicitudInputType>> { Name = "solicitud" }
                ),
                resolve: context =>
                {
                    var solicitud = context.GetArgument<Solicitud>("solicitud");
                    return contextServiceLocator.SolicitudesRepository.Add(solicitud);
                });
        }
    }
}

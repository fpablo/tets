﻿using GraphQL;
using GraphQL.Types;

namespace TSVacante.Api.Models
{
    public class TSVacanteSchema : Schema
    {
        public TSVacanteSchema (IDependencyResolver resolver): base(resolver)
        {
            Query = resolver.Resolve<TSVacanteQuery>();
            Mutation = resolver.Resolve<TSVacanteMutation>();
        }
    }
}

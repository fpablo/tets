﻿using GraphQL.Types;

namespace TSVacante.Api.Models
{
    public class AreaInputType : InputObjectGraphType
    {
        public AreaInputType()
        {
            Name = "AreaInput";
            Field<NonNullGraphType<IntGraphType>>("id");
            Field<NonNullGraphType<StringGraphType>>("nombre");
            Field<NonNullGraphType<StringGraphType>>("descripcion");
            Field<NonNullGraphType<StringGraphType>>("codigo_interno");
            Field<NonNullGraphType<IntGraphType>>("status");
            Field<NonNullGraphType<IntGraphType>>("deleted");
        }
    }
}

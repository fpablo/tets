﻿using GraphQL.Types;
using TSVacante.Api.Helpers;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Api.Models
{
    public class RegionInputType : InputObjectGraphType
    {
        public RegionInputType()
        {
            Name = "RegionInput";
            Field<NonNullGraphType<IntGraphType>>("id");
            Field<NonNullGraphType<StringGraphType>>("nombre");
            Field<NonNullGraphType<StringGraphType>>("codigo_interno");
            Field<NonNullGraphType<IntGraphType>>("status");
            Field<NonNullGraphType<IntGraphType>>("deleted");
            Field<NonNullGraphType<IntGraphType>>("ou_id");
        }
    }
}

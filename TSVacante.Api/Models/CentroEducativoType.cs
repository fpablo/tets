﻿using GraphQL.Types;
using TSVacante.Api.Helpers;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Api.Models
{
    public class CentroEducativoType : ObjectGraphType<CentroEducativo>
    {
        public CentroEducativoType (ContextServiceLocator contextServiceLocator)
        {
            Field(x => x.Id);
            Field(x => x.Nombre);
            Field(x => x.Codigo_interno);
            Field(x => x.Status);
            Field(x => x.Deleted);
            Field(x => x.Region_id);
        }
    }
}

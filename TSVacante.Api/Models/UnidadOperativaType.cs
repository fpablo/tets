﻿using GraphQL.Types;
using TSVacante.Api.Helpers;
using TSVacante.Core.Data;
using TSVacante.Core.Models;

namespace TSVacante.Api.Models
{
    public class UnidadOperativaType : ObjectGraphType<UnidadOpetativa>
    {
        public UnidadOperativaType(ContextServiceLocator contextServiceLocator)
        {
            Field(x => x.Id);
            Field(x => x.Nombre);
            Field(x => x.Codigo_interno);
            Field(x => x.Status);
            Field(x => x.Deleted);
        }
    }
}

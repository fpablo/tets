﻿using GraphQL.Types;

namespace TSVacante.Api.Models
{
    public class UnidadOperativaInputType : InputObjectGraphType
    {
        public UnidadOperativaInputType()
        {
            Name = "UnidadOperativaInput";
            Field<NonNullGraphType<IntGraphType>>("id");
            Field<NonNullGraphType<StringGraphType>>("nombre");
            Field<NonNullGraphType<StringGraphType>>("codigo_interno");
            Field<NonNullGraphType<IntGraphType>>("status");
            Field<NonNullGraphType<IntGraphType>>("deleted");
        }
    }
}

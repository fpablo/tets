﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TSVacante.Core.Models
{
    public class Solicitud
    {
        public int Id { get; set; }
        public string Nombre_puesto { get; set; }
        public int Area_id { get; set; }
        public string Departamento { get; set; }
        public string Puesto_reporta { get; set; }
        public string Correo_jefe_directo { get; set; }
        public int Ou_id { get; set; }
        public int Region_id { get; set; }
        public int Centro_id { get; set; }
        public string Justificacion_operativa { get; set; }

        public string Justificacion_estrategica { get; set; }

        public bool En_presupuesto { get; set; }

        public string Objetivo_puesto { get; set; }

        public string Carrera { get; set; }

        //public int Experiencia_minima { get; set; }

        public string Conocimientos_tecnicos { get; set; }

        public string Sector_preferencia { get; set; }

        //public int Edad { get; set; }

        public string Equipos { get; set; }

        public string Comportamientos_mes_1 { get; set; }

        public string Aspectos_descarte { get; set; }

        public DateTime Fecha { get; set; }

        public DateTime Fecha_actualizacion { get; set; }

        //public double Sueldo_propuesto { get; set; }

        public string Nivel { get; set; }

        //public double Sueldo_autorizado { get; set; }

        public int Deleted { get; set; }

        public int Status_id { get; set; }
        public int Tipo_solicitud_id { get; set; }
        public int Usuario_id { get; set; }
    }
}

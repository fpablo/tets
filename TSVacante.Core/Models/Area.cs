﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TSVacante.Core.Models
{
    public class Area
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Codigo_interno { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
    }
}

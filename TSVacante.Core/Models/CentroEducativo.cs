﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSVacante.Core.Models
{
    public class CentroEducativo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Codigo_interno { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
        public int Region_id { get; set; }
    }
}

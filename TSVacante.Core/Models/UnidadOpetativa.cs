﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSVacante.Core.Models
{
    public class UnidadOpetativa
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Codigo_interno { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSVacante.Core.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public int Status { get; set; }
        public int Deleted { get; set; }
    }
}

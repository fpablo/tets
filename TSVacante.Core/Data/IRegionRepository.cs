﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TSVacante.Core.Models;

namespace TSVacante.Core.Data
{
    public interface IRegionRepository
    {
        Task<Region> Get(int id);
        Task<List<Region>> All();
        Task<Region> Add(Region region);
    }
}

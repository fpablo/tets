﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TSVacante.Core.Models;

namespace TSVacante.Core.Data
{
    public interface ICentroEducativoRepository
    {
        Task<CentroEducativo> Get(int id);
        Task<List<CentroEducativo>> All();
        Task<CentroEducativo> Add(CentroEducativo centroEducativos);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TSVacante.Core.Models;


namespace TSVacante.Core.Data
{
    public interface ISolicitudRepository
    {
        Task<Solicitud> Get(int id);
        Task<List<Solicitud>> All();
        Task<Solicitud> Add(Solicitud solicitudes);
    }
}

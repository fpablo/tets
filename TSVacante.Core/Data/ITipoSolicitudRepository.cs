﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TSVacante.Core.Models;

namespace TSVacante.Core.Data
{
    public interface ITipoSolicitudRepository
    {
        Task<TipoSolicitud> Get(int id);
        Task<List<TipoSolicitud>> All();
        Task<TipoSolicitud> Add(TipoSolicitud tipoSolicitud);
    }
}

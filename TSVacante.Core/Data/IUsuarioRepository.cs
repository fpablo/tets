﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TSVacante.Core.Models;

namespace TSVacante.Core.Data
{
    public interface IUsuarioRepository
    {
        Task<Usuario> Get(int id);
        Task<List<Usuario>> All();
        Task<Usuario> Add(Usuario usuario);
    }
}

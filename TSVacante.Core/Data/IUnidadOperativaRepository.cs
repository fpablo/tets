﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TSVacante.Core.Models;

namespace TSVacante.Core.Data
{
    public interface IUnidadOperativaRepository
    {
        Task<UnidadOpetativa> Get(int id);
        Task<List<UnidadOpetativa>> All();
        Task<UnidadOpetativa> Add(UnidadOpetativa unidadesOpetativas);
    }
}

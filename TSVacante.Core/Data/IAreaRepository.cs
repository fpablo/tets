﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TSVacante.Core.Models;

namespace TSVacante.Core.Data
{
    public interface IAreaRepository
    {
        Task<Area> Get(int id);
        Task<List<Area>> All();
        Task<Area> Add(Area area);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TSVacante.Core.Models;

namespace TSVacante.Core.Data
{
    public interface IEstatusRepository
    {
        Task<Estatus> Get(int id);
        Task<List<Estatus>> All();
        Task<Estatus> Add(Estatus status);
    }
}
